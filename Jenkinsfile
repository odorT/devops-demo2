pipeline{
    environment{
        registry_url = 'http://localhost:5050/root/demo2/'
        registry_header = 'localhost:5050/root/demo2/'
        image_app = "petclinic-app"
        image_db = "petclinic-db"
        build_tag = '2'
        registryCredential = 'gitlab'
    }
    agent any
    stages{
        stage("Build"){
            steps{
                echo "==== Building images ===="
                updateGitlabCommitStatus name: 'build', state: 'pending'
                sh 'docker-compose build'                
            }
            post{
                always{
                    echo "==== Finished image building step ===="
                }
                success{
                    echo "==== Image Building finished successfully ===="
                    updateGitlabCommitStatus name: 'build', state: 'success'
                    sh "docker tag petclinic-app ${registry_header}${image_app}:${build_tag}"
                    sh "docker tag petclinic-db ${registry_header}${image_db}:${build_tag}"
                }
                failure{
                    echo "==== Image Building failed  ===="
                    updateGitlabCommitStatus name: 'build', state: 'failed'
                }
            }
        }
        stage('Test'){
            steps{
                echo '==== Running images ===='
                updateGitlabCommitStatus name: 'test', state: 'pending'
                sh 'docker-compose --env-file .env.dev --compatibility up -d'
                
                echo "==== Executing health-check ====="
                sh './health-check.sh'  

                // echo '==== Executing Vulnerability testing ===='
            }
            post{
                always{
                    echo 'Finished testing step'
                    sh 'docker-compose --env-file .env.dev --compatibility down'
                }
                success{
                    echo 'Testing step finished successfully'
                    updateGitlabCommitStatus name: 'test', state: 'success'
                }
                failure{
                    echo 'Testing step failed'
                    updateGitlabCommitStatus name: 'test', state: 'failed'
                }
            }
        }
        stage('Deploy'){
            steps{
                echo '==== Deploying images ===='
                updateGitlabCommitStatus name: 'deploy', state: 'pending'
                sh 'docker logout'
                script{
                    docker.withRegistry(registry_url, registryCredential){
                        sh "docker push ${registry_header}${image_app}:${build_tag}"
                        sh "docker push ${registry_header}${image_db}:${build_tag}"
                    }
                }
            }
            post{
                always{
                    echo "==== Deployment finished ====="
                }
                success{
                    echo "==== Deployment finished successfully ===="
                    updateGitlabCommitStatus name: 'deploy', state: 'success'
                }
                failure{
                    echo "==== Deployment failed ====="
                    updateGitlabCommitStatus name: 'deploy', state: 'failed'
                }
            }
        }
    }
    post{
        always{
            echo "==== Pipeline execution finished ====="
        }
        success{
            echo "==== Pipeline executed successfully ===="
        }
        failure{
            echo "==== Pipeline execution failed ====="
        }
    }
}

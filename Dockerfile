# Package generation
FROM maven:3.6.1-jdk-8-alpine AS MAVEN_BUILD

WORKDIR /build

ENV BUILD_BRANCH dev
COPY docker-script.sh /build
RUN sh docker-script.sh

# COPY az-devops1/ .

RUN mvn dependency:go-offline
RUN mvn package -DskipTests -Dcheckstyle.skip

# Build image
FROM openjdk:8-jre-alpine3.9

ENV HOME /home/$USER
ARG USER=app
ARG JAR_FILE=/build/target/*.jar

RUN apk add --update sudo
RUN adduser -D $USER && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER && chmod 0440 /etc/sudoers.d/$USER

USER $USER
WORKDIR $HOME
RUN sudo chown -R $USER:$USER $HOME

COPY --from=MAVEN_BUILD ${JAR_FILE} app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]


#!/bin/bash

up="{\"status\":\"UP\"}"
timeout=10

for i in {1..25}; do
    if [[ $i != 'end-of-loop' ]]; then
        status=$(curl -s http://localhost:8081/actuator/health)
        if [[ "$up" == "$status" ]]; then
            exit 0
            break
        else
            echo "sleeping ${timeout} seconds for ${i}th time"
            sleep ${timeout};
        fi
    fi
    if [[ $i == 'end-of-loop' ]]; then
        exit 1
    fi
done

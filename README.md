# Repository for [demo2](https://gitlab.com/dan-it/az-groups/az-devops1/-/blob/master/Tasks/Demo2.md). Before continuing, please read the notes below.
## Notes

- Use docker-compose version 1.28.6, Docker version 20.10.5

- In the image build stage I get the source code of application with 2 ways: (1)cloning inside image, and (2)copying from folder. Comment lines 6,7,8 and uncomment line 10 to copy from folder. For (1), I have created separate repository, you can define the branch you want to clone inside the Dockerfile. For (2), you can set branch either in Jenkins Pipeline configuration, or just change the line 2 in JobDSL. 

- The source code of the appication is located in [demo2-source](https://gitlab.com/odorT/demo2-source)

- [local-registry/docker-compose.yml](https://gitlab.com/odorT/devops-demo2/-/blob/master/local-registry/docker-compose.yml) file will create 3 containers, one for gitlab server(which will host private docker container registry), another for minio object storage(which will store the images pushed to gitlab image registry) and the last for minio client.

- [env.dev](https://gitlab.com/odorT/devops-demo2/-/blob/master/.env.dev) is for storing environment variables

- [docker-compose.yml](https://gitlab.com/odorT/devops-demo2/-/blob/master/docker-compose.yml) will create 2 images, the application image and database image.

- [docker-script.sh](https://gitlab.com/odorT/devops-demo2/-/blob/master/docker-script.sh) is for branch-cloning inside the multi-stage build

- [Dockerfile](https://gitlab.com/odorT/devops-demo2/-/blob/master/Dockerfile) is for application image. I used multi-stage build, by this I achieve better run time performance, better organization and minimal size of the final image/container. 

- [Dockerfile.mysql](https://gitlab.com/odorT/devops-demo2/-/blob/master/Dockerfile.mysql) is for database. I created this file to tag image as petclinic-db inside [docker-compose]().yml

- [health-check.sh](https://gitlab.com/odorT/devops-demo2/-/blob/master/health-check.sh) is for checking '/actuator/health' url endpoint of application to assure that the website is 100% working.

- [Jenkinsfile](https://gitlab.com/odorT/devops-demo2/-/blob/master/Jenkinsfile). 

- [JobDSL](https://gitlab.com/odorT/devops-demo2/-/blob/master/JobDSL) is for creating jobs. You can define which branch to use here in line 2.

- [az-devops1/](https://gitlab.com/odorT/devops-demo2/-/blob/master/az-devops1/) is for storing application source files

## Configuration
Run this to create a network that will be used in mysql-app connection:  
`docker network create --driver=bridge --subnet=192.168.0.0/24 --ip-range=192.168.0.1/29 --gateway=192.168.0.6  petclinic-network`

If you want to run images, use following command:  
`docker-compose build`  
`docker-compose --env-file .env.dev --compatibility up`  
For gitlab server, run following inside local-registry/ folder:  
`docker-compose up`

## Port configuration
localhost:2224  gitlab-ssh-shell  
localhost:3306  mysql  
localhost:8080  jenkins  
localhost:8081  petclinic  
localhost:8083  gitlab  
localhost:9000  minio  